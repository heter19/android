package com.example.la;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.la.R;

public class SecondActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

    }

    public void pole(View view) {
        EditText editText3 = (EditText) findViewById(R.id.editText3);
        EditText editText2 = (EditText) findViewById(R.id.editText2);
        Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);

        //wyciagam wartosci
        String imie = String.valueOf(editText3.getText());
        String wiek = String.valueOf(editText2.getText());
        String pozycja = String.valueOf(spinner2.getSelectedItem());

        //przechodze do 3
        Intent intent = new Intent(this, ThirdActivity.class);
        intent.putExtra("imie", imie);
        intent.putExtra("wiek", wiek);
        intent.putExtra("pozycja", pozycja);
        startActivity(intent);



    }
}

