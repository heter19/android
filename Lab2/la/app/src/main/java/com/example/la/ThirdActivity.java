package com.example.la;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.la.R;

public class ThirdActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        Intent i = getIntent();

        String wiek = i.getStringExtra("wiek");
        String imie = i.getStringExtra("imie");
        String pozycja = i.getStringExtra("pozycja");

        String enterKey = System.getProperty("line.separator");
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText("Piłkarz: " + imie + enterKey + "Wiek: " + wiek + enterKey + "Pozycja: " + pozycja);
        
    }
}
