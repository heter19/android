package com.example.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickDodawanie(View view){
        TextView calculate =(TextView) findViewById(R.id.wynik);
        Spinner spinner1 = findViewById(R.id.spinner);
        Spinner spinner2 = findViewById(R.id.spinner2);

        int firstNumber = Integer.parseInt(spinner1.getSelectedItem().toString());
        int secondNumber = Integer.parseInt(spinner2.getSelectedItem().toString());
        int result = firstNumber + secondNumber;
            calculate.setText("Wynik to: " + result);
    }

    public void onClickOdejmowanie(View view){
        TextView calculate =(TextView) findViewById(R.id.wynik);
        Spinner spinner1 = findViewById(R.id.spinner);
        Spinner spinner2 = findViewById(R.id.spinner2);

        int firstNumber = Integer.parseInt(spinner1.getSelectedItem().toString());
        int secondNumber = Integer.parseInt(spinner2.getSelectedItem().toString());
        int result = firstNumber - secondNumber;
        calculate.setText("Wynik to: " + result);
    }
    public void onClickMnozenie(View view){
        TextView calculate =(TextView) findViewById(R.id.wynik);
        Spinner spinner1 = findViewById(R.id.spinner);
        Spinner spinner2 = findViewById(R.id.spinner2);

        int firstNumber = Integer.parseInt(spinner1.getSelectedItem().toString());
        int secondNumber = Integer.parseInt(spinner2.getSelectedItem().toString());
        int result = firstNumber * secondNumber;
        calculate.setText("Wynik to: " + result);
    }
    public void onClickDzielenie(View view){
        TextView calculate =(TextView) findViewById(R.id.wynik);
        Spinner spinner1 = findViewById(R.id.spinner);
        Spinner spinner2 = findViewById(R.id.spinner2);

        int firstNumber = Integer.parseInt(spinner1.getSelectedItem().toString());
        int secondNumber = Integer.parseInt(spinner2.getSelectedItem().toString());
        int result = firstNumber / secondNumber;
        calculate.setText("Wynik to: " + result);
    }
}
