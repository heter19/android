package com.example.lab4;

public class Napastnicy {
    private String name;
    private int imgRes;
    private String opis;

    public String getOpis() {
        return opis;
    }

    public static final Napastnicy [] NAPASTNICIES = {
            new Napastnicy ("Cristiano Ronaldo", R.drawable.ronaldo, "Portugalski piłkarz występujący na pozycji napastnika we włoskim klubie Juventus"),
            new Napastnicy ("Kylian Mbappe", R.drawable.mbapp, "Francuski piłkarz kameruńskiego pochodzenia występujący na pozycji napastnika we francuskim klubie Paris Saint-Germain"),

    };

    public String getName() {
        return name;
    }


    public int getImgRes() {
        return imgRes;
    }

    public Napastnicy (String name, int imgRes, String opis) {
        this.name = name;
        this.opis = opis;

        this.imgRes = imgRes;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
