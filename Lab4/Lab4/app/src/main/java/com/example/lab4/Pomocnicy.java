package com.example.lab4;

public class Pomocnicy {
    private String name;

    private int imgRes;

    public Pomocnicy(String name, int imgRes) {
        this.name = name;
        this.imgRes = imgRes;
    }

    public String getName() {
        return name;
    }


    public int getImgRes() {
        return imgRes;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public static final Pomocnicy[] POMOCNICIES = {
            new Pomocnicy("Kevin De Bruyne", R.drawable.kevin),
            new Pomocnicy("Christian Eriksen", R.drawable.eriksen),

    };
}
