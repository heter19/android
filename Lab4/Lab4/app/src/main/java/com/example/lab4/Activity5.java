package com.example.lab4;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class Activity5 extends AppCompatActivity {

    public static final String EXTRA_NAPASTNIK = " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_5);
        int napastnik = (Integer) getIntent().getExtras().get(EXTRA_NAPASTNIK);
        Napastnicy napastnicy = Napastnicy.NAPASTNICIES[napastnik];

        TextView name = (TextView) findViewById(R.id.textView);
        name.setText(napastnicy.getName());


        ImageView photo = (ImageView) findViewById(R.id.photo1);
        photo.setImageResource(napastnicy.getImgRes());
        photo.setContentDescription(napastnicy.getName());

        TextView opis = (TextView) findViewById(R.id.textView2);
        opis.setText(napastnicy.getOpis());


    }
}
