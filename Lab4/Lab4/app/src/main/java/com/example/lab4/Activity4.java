package com.example.lab4;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class Activity4 extends AppCompatActivity {


    public static final String EXTRA_POMOCNICY = " ";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4);


        int pomocnicy2 = (Integer) getIntent().getExtras().get(EXTRA_POMOCNICY);
        Pomocnicy pomocnicy = Pomocnicy.POMOCNICIES[pomocnicy2];
        TextView name = (TextView) findViewById(R.id.textView2);
        name.setText(pomocnicy.getName());

        ImageView photo = (ImageView) findViewById(R.id.photo);
        photo.setImageResource(pomocnicy.getImgRes());
        photo.setContentDescription(pomocnicy.getName());


    }


}
